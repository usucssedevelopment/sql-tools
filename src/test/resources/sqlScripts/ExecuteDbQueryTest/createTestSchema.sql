DROP TABLE IF EXISTS db_query_test.person;
DROP SCHEMA IF EXISTS db_query_test CASCADE;

CREATE SCHEMA db_query_test;

CREATE TABLE db_query_test.person (
  person_id integer PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
  last_name VARCHAR(40)
);

INSERT INTO db_query_test.person (person_id, first_name, last_name)
            VALUES (1, 'Linda', 'Larson');
INSERT INTO db_query_test.person (person_id, first_name, last_name)
            VALUES (2, 'Kim', 'Jones');
INSERT INTO db_query_test.person (person_id, first_name, last_name)
            VALUES (3, 'Jimmy', 'Johns');
INSERT INTO db_query_test.person (person_id, first_name, last_name)
            VALUES (4, 'Ivan', 'Iceland');
