DROP TABLE IF EXISTS db_result_set_extension_test.person;
DROP SCHEMA IF EXISTS db_result_set_extension_test CASCADE;

CREATE SCHEMA db_result_set_extension_test;

CREATE TABLE db_result_set_extension_test.person (
  person_id integer PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
  last_name VARCHAR(40),
  birth_date TIMESTAMP,
  death_day SMALLINT,
  death_month SMALLINT,
  death_year SMALLINT,
  score INTEGER,
  scored_on DATE
);

INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (1, 'Linda', 'Lar son', '20200102 03:04:05', 1, 2, 2021, 13435, '2020-02-02');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (2, 'Kim', 'Jone s', '20200203 04:05:06', null, 2, 2021, 98324, '2020-02-05');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (3, 'Jim my', 'Johns', '20200304 05:06:07', null, null, 2021, 73212, '2020-02-07');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (4, 'I van', 'Iceland', '20200405 06:07:08', 0, 0, 0, 61345, '2020-05-12');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (5, 'Nick', 'Nash', '20200506 07:08:09', null, null, null, 23455, '2020-06-07');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (6, 'Matt', 'Marvel', '20200607 08:09:10', 1, null, null, 77523, '2020-08-09');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (7, 'Paula', 'Peters', '20200708 09:10:11', 1, 2, null, 175, '2020-12-06');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (8, 'Shara', 'Smith', '20200809 10:12:12', null, 2, null, 13455, '2020-10-01');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (9, 'Tim', 'Thompson', '20200910 11:12:13', 30, 2, 2021, 17564, '2020-11-12');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (10, 'Vi', 'Vinsen', '20201011 12:13:14', 1, 13, 10000, 62346, '2020-03-05');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (11, null, null, null, null, null, null, null, null);
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (12, 'Vi', 'Vinsen', '20201011 12:13:14', 31, 2, 2021, 62346, '2020-02-15');
INSERT INTO db_result_set_extension_test.person (person_id, first_name, last_name, birth_date, death_day, death_month, death_year, score, scored_on)
            VALUES (13, 'Vi', 'Vinsen', '20201011 12:13:14', 31, 13, 2021, 62346, '2020-12-31');