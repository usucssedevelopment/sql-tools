DROP TABLE IF EXISTS db_prepared_statement_test.person;
DROP SCHEMA IF EXISTS db_prepared_statement_test CASCADE;

CREATE SCHEMA db_prepared_statement_test;

CREATE TABLE db_prepared_statement_test.person (
  person_id integer PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
  last_name VARCHAR(40)
);

INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)
            VALUES (1, 'Linda', 'Larson');
INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)
            VALUES (2, 'Kim', 'Jones');
INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)
            VALUES (3, 'Jimmy', 'Johns');
INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)
            VALUES (4, 'Ivan', 'Iceland');
