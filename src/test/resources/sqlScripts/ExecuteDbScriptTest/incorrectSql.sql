DROP SCHEMA IF EXISTS sqlTest CASCADE;

CREATE SCHEMA sqlTest;

CREATE TABLE sqlTest.patients (
  usiis_id NUMERIC(12) PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
);

-- Linda Larson
INSERT INTO sqlTest.patients (usiis_id,
                      first_name, middle_name, last_name, suffix,
                      ssn, dob, birth_state, gender,
                      address_street, address_street2, address_city, address_state, address_zip, country_code,
                      phone_number,
                      adopt_flg, medicaid_id)
          VALUES     ('18334251',
                      'Linda', null, 'Larson', null,
                      '400101000', to_date('2014-10-01', 'YYYY-MM-DD'), 'UT', 'F',
                      '100 Main St.', null, 'Testville', 'UT', '12345', 'US',
                      '4357891234',
                      null, '0310811234');
