DROP TABLE IF EXISTS db_script_test.person;
DROP SCHEMA IF EXISTS db_script_test CASCADE;

CREATE SCHEMA db_script_test;

CREATE TABLE db_script_test.person (
  person_id integer PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
  last_name VARCHAR(40)
);
