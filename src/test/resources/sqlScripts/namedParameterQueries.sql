DROP SCHEMA IF EXISTS test CASCADE;

CREATE SCHEMA test;

CREATE TABLE test.people (
  person_id NUMERIC(12) PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
  last_name VARCHAR(40),
  age INTEGER
);

-- Shauna Thomson
INSERT INTO test.people (person_id,
                      first_name, last_name, age)
          VALUES     ('18334261',
                      @firstName1, @lastName1, 43);

-- Kay Taylor
INSERT INTO test.people (person_id,
                      first_name, last_name, age)
          VALUES     ('18338662',
                      'Kay', @lastName2, @age2);
