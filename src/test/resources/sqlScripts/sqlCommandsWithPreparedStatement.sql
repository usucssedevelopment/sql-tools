DROP SCHEMA IF EXISTS test CASCADE;

CREATE SCHEMA test;

CREATE TABLE test.persons (
  person_id NUMERIC(12) PRIMARY KEY NOT NULL,
  first_name VARCHAR(40),
  last_name VARCHAR(40),
  age INTEGER
);

-- Adam Anderson
INSERT INTO test.persons (person_id,
                      first_name, last_name, age)
          VALUES     ('18334256',
                      'Adam',@lastName1, @age1);

-- Mary Ann Matthews
INSERT INTO test.persons (person_id,
                      first_name, last_name, age)
          VALUES     ('18338657',
                      @firstName2,'Matthews',@age2);
