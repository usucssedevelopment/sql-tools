DROP SEQUENCE IF EXISTS db_sequence_test.widget_id_seq;
DROP SCHEMA IF EXISTS db_sequence_test CASCADE;

CREATE SCHEMA db_sequence_test;
CREATE SEQUENCE db_sequence_test.widget_id_seq;
