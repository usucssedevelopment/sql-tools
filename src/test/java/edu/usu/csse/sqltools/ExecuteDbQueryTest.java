package edu.usu.csse.sqltools;

import org.postgresql.util.PSQLException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class ExecuteDbQueryTest {
    private Connection conn;

    @BeforeEach
    public void setUp() throws Exception {
        this.conn = TestingUtils.getConnection();
        int count = ExecuteDbScript.executeFromResource(
                conn, "sqlScripts/ExecuteDbQueryTest/createTestSchema.sql", null);
        assertEquals(0, count);
    }

    @Test
    public void testExecuteWithoutParameters() throws Exception {

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_query_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals(1, resultSet.getInt(1));
        assertEquals("Linda", resultSet.getString(2));
        assertEquals("Larson", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(2, resultSet.getInt(1));
        assertEquals("Kim", resultSet.getString(2));
        assertEquals("Jones", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(4, resultSet.getInt(1));
        assertEquals("Ivan", resultSet.getString(2));
        assertEquals("Iceland", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void testExecuteWithParameters() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", 3));

        String query = "SELECT person_id, first_name, last_name\n" +
                " FROM db_query_test.person\n" +
                " WHERE person_id=@id";
        ResultSet resultSet = ExecuteDbQuery.execute(conn, query, parameters);

        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromResource() throws Exception {
        ResultSet resultSet = ExecuteDbQuery.executeFromResource(conn,
                "sqlScripts/ExecuteDbQueryTest/queryWithoutParameters.sql",
                null);
        assertTrue(resultSet.next());
        assertEquals(1, resultSet.getInt(1));
        assertEquals("Linda", resultSet.getString(2));
        assertEquals("Larson", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(2, resultSet.getInt(1));
        assertEquals("Kim", resultSet.getString(2));
        assertEquals("Jones", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(4, resultSet.getInt(1));
        assertEquals("Ivan", resultSet.getString(2));
        assertEquals("Iceland", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromResourceWithParameters() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", 3));

        ResultSet resultSet = ExecuteDbQuery.executeFromResource(conn,
                "sqlScripts/ExecuteDbQueryTest/queryWithOneParameter.sql",
                parameters);
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromFile() throws Exception {
        ResultSet resultSet = ExecuteDbQuery.executeFromFile(conn,
                "src/test/resources/sqlScripts/ExecuteDbQueryTest/queryWithoutParameters.sql",
                null);
        assertTrue(resultSet.next());
        assertEquals(1, resultSet.getInt(1));
        assertEquals("Linda", resultSet.getString(2));
        assertEquals("Larson", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(2, resultSet.getInt(1));
        assertEquals("Kim", resultSet.getString(2));
        assertEquals("Jones", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(4, resultSet.getInt(1));
        assertEquals("Ivan", resultSet.getString(2));
        assertEquals("Iceland", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromFileWithParameters() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", 3));
        parameters.add(new Parameter("firstName", "Jimmy"));
        ResultSet resultSet = ExecuteDbQuery.executeFromFile(conn,
                "src/test/resources/sqlScripts/ExecuteDbQueryTest/queryWithTwoParameters.sql",
                parameters);
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromFileThrowsWhenPathInvalid() throws Exception {
        assertThrows(FileNotFoundException.class, () -> {
            ExecuteDbQuery.executeFromFile(conn, "nonExistentFile.txt", null);
        });
    }

    @Test
    public void connectionIsNull() throws Exception {
        assertNull(ExecuteDbQuery.execute(null, "SELECT 1;", null));
        assertNull(ExecuteDbQuery.executeFromResource(null,
                "sqlScripts/ExecuteDbQueryTest/queryWithOneParameter.sql",
                null));
        assertNull(ExecuteDbQuery.executeFromFile(null,
                "src/test/resources/sqlScripts/ExecuteDbQueryTest/queryWithOneParameter.sql",
                null));
    }

    @Test
    public void scriptResourceIsIncorrect() throws Exception {
        try {
            ExecuteDbScript.executeFromResource(conn, "", null);
            fail("Expected exception not thrown");
        } catch (PSQLException e) {
            //ignore
        }

        int count = ExecuteDbScript.executeFromResource(conn, "sqlScripts/emptyFile.sql", null);
        assertEquals(0, count);

        try {
            ExecuteDbScript.executeFromResource(conn, "anyRandomFile", null);
            fail("Expected exception not thrown");
        } catch (IOException e) {
            // ignore
        }

        try {
            ExecuteDbScript.executeFromResource(conn, "sqlScripts/ExecuteDbQueryTest/incorrectSql.sql", null);
            fail("Expected exception not thrown");
        } catch (PSQLException e) {
            // ignore
        }
    }

    @Test
    public void numberOfParametersGivenAreLessThanExpected() throws Exception {
        List<Parameter> params = new ArrayList<>();
        params.add(new Parameter("firstName", "Linda", Types.VARCHAR));

        assertThrows (PSQLException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbQueryTest/queryWithTwoParameters.sql",
                    params);
        });
    }

    @Test
    public void numberOfParametersGivenAreMoreThanExpected() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", "6", Types.VARCHAR));
        parameters.add(new Parameter("firstName", "Peter"));
        parameters.add(new Parameter("extra", "bad"));

        assertThrows(IllegalArgumentException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbQueryTest/queryWithTwoParameters.sql",
                    parameters);
        });
    }

    @Test
    public void incorrectTypeOfParameter() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", "6", Types.VARCHAR));

        assertThrows(PSQLException.class, ()-> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbQueryTest/queryWithOneParameter.sql",
                    parameters);
        });
    }

    @Test
    public void paramsAreEmptyForPreparedStatement() throws Exception {
        List<Parameter> parameters = new ArrayList<>();

        assertThrows(PSQLException.class, ()-> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbQueryTest/queryWithOneParameter.sql",
                    parameters);
        });
    }

    @Test
    public void paramsAreNullForPreparedStatement() throws Exception {
        assertThrows(PSQLException.class, ()-> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbQueryTest/queryWithOneParameter.sql",
                    null);
        });
    }

    @Test
    public void paramsAreNotNullEvenWhenPreparedStatementIsNotRequired() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("firstName", "Linda", Types.VARCHAR));

        assertThrows(IllegalArgumentException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbQueryTest/queryWithoutParameters.sql",
                    parameters);
        });
    }

    @AfterEach
    public void tearDown() throws Exception {
        ExecuteDbScript.executeFromResource(
                conn,
                "sqlScripts/ExecuteDbQueryTest/dropTestSchema.sql",
                null);
    }


}