package edu.usu.csse.sqltools;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DbSequenceTest {

    private Connection conn;

    @BeforeEach
    public void setUp() throws Exception {
        this.conn = TestingUtils.getConnection();
        int count = ExecuteDbScript.executeFromResource(
                conn, "sqlScripts/DbSequenceTest/createTestSchema.sql", null);
        assertEquals(0, count);
    }

    @Test
    public void checkEverything() throws SQLException {
        long a = DbSequence.getNextValue(conn,"db_sequence_test.widget_id_seq");
        long b = DbSequence.getNextValue(conn,"db_sequence_test.widget_id_seq");
        assertEquals(a + 1, b);
        b = DbSequence.getNextValue(conn,"db_sequence_test.widget_id_seq");
        assertEquals(a + 2, b);
        b = DbSequence.getCurrentValue(conn,"db_sequence_test.widget_id_seq");
        assertEquals(a + 2, b);

        DbSequence.setValue(conn, "db_sequence_test.widget_id_seq", 100);
        b = DbSequence.getCurrentValue(conn,"db_sequence_test.widget_id_seq");
        assertEquals(100, b);
        b = DbSequence.getNextValue(conn,"db_sequence_test.widget_id_seq");
        assertEquals(101, b);

        try {
            DbSequence.getNextValue(conn,"bad");
        } catch (SQLException e) {
            // ignore
        }
    }

    @AfterEach
    public void teardown() throws Exception {
        ExecuteDbScript.executeFromResource(
                conn,
                "sqlScripts/DbSequenceTest/dropTestSchema.sql",
                null);
    }
}