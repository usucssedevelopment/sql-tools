package edu.usu.csse.sqltools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ConstantConditions")
public class ResultSetExtensionTest {

    private Connection conn;

    @BeforeEach
    public void setUp() throws Exception {
        this.conn = TestingUtils.getConnection();
        int count = ExecuteDbScript.executeFromResource(
                conn, "sqlScripts/ResultSetExtensionTest/createTestSchema.sql", null);
        assertEquals(0, count);
    }

    @Test
    public void testGetFields() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT *\n" +
                " FROM db_result_set_extension_test.person WHERE person_id=1\n");
        assertTrue(resultSet.next());
        assertEquals("Linda", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", false, true));
        assertEquals("Larson", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", false, true));
        assertEquals("2020-01-02", ResultSetExtension.getDateField(resultSet, "birth_date").toString());
        assertEquals(1, (short) ResultSetExtension.getShortField(resultSet, "death_day"));
        assertEquals(2, (short) ResultSetExtension.getShortField(resultSet, "death_month"));
        assertEquals(2021, (short) ResultSetExtension.getShortField(resultSet, "death_year"));
        assertEquals(13435, (int) ResultSetExtension.getIntegerField(resultSet, "score"));
        assertEquals(ResultSetExtension.getDateField(resultSet, "scored_on").toString(), "2020-02-02");

        resultSet = ExecuteDbQuery.execute(conn, "SELECT *\n" +
                " FROM db_result_set_extension_test.person WHERE person_id=11\n");
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getTrimmedStringField(resultSet, "first_name", false, false));
        assertNull(ResultSetExtension.getTrimmedStringField(resultSet, "last_name", false, false));
        assertNull(ResultSetExtension.getDateField(resultSet, "birth_date"));
        assertNull(ResultSetExtension.getShortField(resultSet, "death_day"));
        assertNull(ResultSetExtension.getShortField(resultSet, "death_month"));
        assertNull(ResultSetExtension.getShortField(resultSet, "death_year"));
        assertNull(ResultSetExtension.getIntegerField(resultSet, "score"));
        assertNull(ResultSetExtension.getDateField(resultSet, "scored_on"));

        assertNull(ResultSetExtension.getDateField(resultSet, "bad_field"));
        assertNull(ResultSetExtension.getShortField(resultSet, "bad_field"));
        assertNull(ResultSetExtension.getIntegerField(resultSet, "bad_field"));
    }

    @Test
    public void testGetTrimmedStringFieldWithNoConversions() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("Linda", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", false, true));
        assertEquals("Larson", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", false, true));
        assertTrue(resultSet.next());
        assertEquals("Kim", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", false, true));
        assertEquals("Jones", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", false, true));
        assertTrue(resultSet.next());
        assertEquals("Jimmy", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", false, true));
        assertEquals("Johns", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", false, true));
        assertTrue(resultSet.next());
        assertEquals("Ivan", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", false, true));
        assertEquals("Iceland", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", false, true));
    }

    @Test
    public void testGetTrimmedStringFieldUpperCaseConversions() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("LINDA", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, true));
        assertEquals("LARSON", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, true));
        assertTrue(resultSet.next());
        assertEquals("KIM", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, true));
        assertEquals("JONES", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, true));
        assertTrue(resultSet.next());
        assertEquals("JIMMY", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, true));
        assertEquals("JOHNS", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, true));
        assertTrue(resultSet.next());
        assertEquals("IVAN", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, true));
        assertEquals("ICELAND", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, true));
    }

    @Test
    public void testGetTrimmedStringFieldForNullFields() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person WHERE person_id=11\n");
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, false));
        assertNull(ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, false));
    }

    @Test
    public void testStringFieldUpperCaseConversions() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("LINDA", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, false));
        assertEquals("LAR SON", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, false));
        assertTrue(resultSet.next());
        assertEquals("KIM", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, false));
        assertEquals("JONE S", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, false));
        assertTrue(resultSet.next());
        assertEquals("JIM MY", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, false));
        assertEquals("JOHNS", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, false));
        assertTrue(resultSet.next());
        assertEquals("I VAN", ResultSetExtension.getTrimmedStringField(resultSet, "first_name", true, false));
        assertEquals("ICELAND", ResultSetExtension.getTrimmedStringField(resultSet, "last_name", true, false));
    }

    @Test
    public void testGetTrimmedStringFieldForBadResultSet() {
        String value = ResultSetExtension.getTrimmedStringField(null, "field1", false, true);
        assertNull(value);
    }

    @Test
    public void testGetTrimmedStringFieldForBadFieldName() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getTrimmedStringField(resultSet, "bad", true, false));
    }


    @Test
    public void testGetNumericFieldAsString() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("1", ResultSetExtension.getNumericFieldAsString(resultSet, "person_id"));
        assertTrue(resultSet.next());
        assertEquals("2", ResultSetExtension.getNumericFieldAsString(resultSet, "person_id"));
        assertTrue(resultSet.next());
        assertEquals("3", ResultSetExtension.getNumericFieldAsString(resultSet, "person_id"));
        assertTrue(resultSet.next());
        assertEquals("4", ResultSetExtension.getNumericFieldAsString(resultSet, "person_id"));
    }

    @Test
    public void testGetNumericFieldAsStringForBadResultSet() {
        String value = ResultSetExtension.getNumericFieldAsString(null, "person_id");
        assertNull(value);
    }

    @Test
    public void testGetNumericFieldAsStringForBadFieldName() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getNumericFieldAsString(resultSet, "bad"));
    }

    @Test
    public void testGetTimestampFieldAsString() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, birth_date\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("2020-01-02T03:04:05", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date"));
        assertTrue(resultSet.next());
        assertEquals("2020-02-03T04:05:06", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date"));
        assertTrue(resultSet.next());
        assertEquals("2020-03-04T05:06:07", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date"));
        assertTrue(resultSet.next());
        assertEquals("2020-04-05T06:07:08", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date"));

        resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, birth_date\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");

        assertTrue(resultSet.next());
        assertEquals("2020-01-02", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date", DateTimeFormatter.ISO_DATE));
        assertTrue(resultSet.next());
        assertEquals("2020-02-03", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date", DateTimeFormatter.ISO_DATE));
        assertTrue(resultSet.next());
        assertEquals("2020-03-04", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date", DateTimeFormatter.ISO_DATE));
        assertTrue(resultSet.next());
        assertEquals("2020-04-05", ResultSetExtension.getTimestampFieldAsString(resultSet, "birth_date", DateTimeFormatter.ISO_DATE));

    }

    @Test
    public void testGetTimestampFieldAsStringForBadResultSet() {
        String value = ResultSetExtension.getTimestampFieldAsString(null, "person_id");
        assertNull(value);
    }

    @Test
    public void testGetTimestampFieldAsStringForBadFieldName() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, birth_date\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getTimestampFieldAsString(resultSet, "bad"));
    }

    @Test
    public void testGetFragmentedDateFieldsAsString() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, death_day, death_month, death_year\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("2021-02-01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("2021-02", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet,  "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("2021", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("--01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("-02-01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("-02", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("2021-02", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("--01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("2021-02", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
        assertTrue(resultSet.next());
        assertEquals("2021--31", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year"));
    }

    @Test
    public void testGetFragmentedDateFieldsAsStringWithInvalidAllows() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, death_day, death_month, death_year\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals("2021-02-01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("2021-02", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet,  "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("2021", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("0000-00-00", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("--01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("-02-01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("-02", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("2021-02-30", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("10000-13-01", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("2021-02-31", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
        assertTrue(resultSet.next());
        assertEquals("2021-13-31", ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "death_year", false));
    }

    @Test
    public void testGetFragmentedDateFieldsAsStringForBadResultSet() {
        String value = ResultSetExtension.getFragmentedDateFieldsAsString(null, "death_day", "death_month", "death_year");
        assertNull(value);
    }

    @Test
    public void testGetFragmentedDateFieldsAsStringForBadFieldNames() throws SQLException {
        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, death_day, death_month, death_year\n" +
                " FROM db_result_set_extension_test.person ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "bad", "death_month", "death_year"));
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "bad", "death_year"));
        assertNull(ResultSetExtension.getFragmentedDateFieldsAsString(resultSet, "death_day", "death_month", "bad"));
    }


    @AfterEach
    public void tearDown() throws Exception {
        ExecuteDbScript.executeFromResource(
                conn, "sqlScripts/ResultSetExtensionTest/dropTestSchema.sql", null);
        conn.close();
    }

}