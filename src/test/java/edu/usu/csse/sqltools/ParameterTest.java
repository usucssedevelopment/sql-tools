package edu.usu.csse.sqltools;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.sql.Types;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ParameterTest {

    @Test
    public void checkConstructorGettersAndSettersForParameterWithName(){
        Parameter parameter0 = new Parameter("firstName", "Linda");
        assertEquals("firstName", parameter0.getParameterName());
        assertEquals(0, parameter0.getParameterIndex());
        assertEquals(Types.VARCHAR, parameter0.getTargetSqlType());
        assertEquals("Linda", parameter0.getValue());
        assertEquals(0, parameter0.getScaleOrLength());

        Parameter parameter1 = new Parameter("id", 10);
        assertEquals("id", parameter1.getParameterName());
        assertEquals(0, parameter1.getParameterIndex());
        assertEquals(Types.INTEGER, parameter1.getTargetSqlType());
        assertEquals(10, parameter1.getValue());
        assertEquals(0, parameter1.getScaleOrLength());

        Parameter parameter2 = new Parameter("numOfDays", 10, Types.INTEGER);
        assertEquals("numOfDays", parameter2.getParameterName());
        assertEquals(0, parameter2.getParameterIndex());
        assertEquals(Types.INTEGER, parameter2.getTargetSqlType());
        assertEquals(10, parameter2.getValue());
        assertEquals(0, parameter2.getScaleOrLength());

        Parameter parameter3 = new Parameter("text",
                new InputStreamReader(new ByteArrayInputStream("stream".getBytes())), Types.CLOB,15);
        assertEquals("text", parameter3.getParameterName());
        assertEquals(0, parameter3.getParameterIndex());
        assertEquals(Types.CLOB, parameter3.getTargetSqlType());
        assertEquals(InputStreamReader.class, parameter3.getValue().getClass());
        assertEquals(15, parameter3.getScaleOrLength());

        parameter3.setParameterName("distance");
        assertEquals("distance", parameter3.getParameterName());

        parameter3.setTargetSqlType(Types.DECIMAL);
        assertEquals(Types.DECIMAL, parameter3.getTargetSqlType());

        parameter3.setValue(1234.56789);
        assertEquals(1234.56789, parameter3.getValue());

        parameter3.setScaleOrLength(5);
        assertEquals(5, parameter3.getScaleOrLength());

        parameter3.setParameterName("binaryNum");
        parameter3.setTargetSqlType(Types.BINARY);
        parameter3.setValue(0b110);
        parameter3.setScaleOrLength(0);

        assertEquals("binaryNum", parameter3.getParameterName());
        assertEquals(Types.BINARY, parameter3.getTargetSqlType());
        assertEquals(0b110, parameter3.getValue());
        assertEquals(0, parameter3.getScaleOrLength());
    }

    @Test
    public void checkConstructorGettersAndSettersForParameterWithIndex(){
        Parameter parameter0 = new Parameter(1, "Linda");
        assertEquals(null, parameter0.getParameterName());
        assertEquals(1, parameter0.getParameterIndex());
        assertEquals(Types.VARCHAR, parameter0.getTargetSqlType());
        assertEquals("Linda", parameter0.getValue());
        assertEquals(0, parameter0.getScaleOrLength());

        Parameter parameter1 = new Parameter(1, 20);
        assertEquals(null, parameter1.getParameterName());
        assertEquals(1, parameter1.getParameterIndex());
        assertEquals(Types.INTEGER, parameter1.getTargetSqlType());
        assertEquals(20, parameter1.getValue());
        assertEquals(0, parameter1.getScaleOrLength());

        Parameter parameter2 = new Parameter(2, 10, Types.INTEGER);
        assertEquals(null, parameter2.getParameterName());
        assertEquals(2, parameter2.getParameterIndex());
        assertEquals(Types.INTEGER, parameter2.getTargetSqlType());
        assertEquals(10, parameter2.getValue());
        assertEquals(0, parameter2.getScaleOrLength());

        Parameter parameter3 = new Parameter(3,
                new InputStreamReader(new ByteArrayInputStream("stream".getBytes())), Types.CLOB,15);
        assertEquals(null, parameter3.getParameterName());
        assertEquals(3, parameter3.getParameterIndex());
        assertEquals(Types.CLOB, parameter3.getTargetSqlType());
        assertEquals(InputStreamReader.class, parameter3.getValue().getClass());
        assertEquals(15, parameter3.getScaleOrLength());

        parameter3.setParameterIndex(7);
        assertEquals(7, parameter3.getParameterIndex());

        parameter3.setTargetSqlType(Types.NUMERIC);
        assertEquals(Types.NUMERIC, parameter3.getTargetSqlType());

        parameter3.setValue(0.123456);
        assertEquals(0.123456, parameter3.getValue());

        parameter3.setScaleOrLength(6);
        assertEquals(6, parameter3.getScaleOrLength());

        parameter3.setParameterIndex(2);
        parameter3.setTargetSqlType(Types.BINARY);
        parameter3.setValue(0b110);
        parameter3.setScaleOrLength(0);

        assertEquals(2, parameter3.getParameterIndex());
        assertEquals(Types.BINARY, parameter3.getTargetSqlType());
        assertEquals(0b110, parameter3.getValue());
        assertEquals(0, parameter3.getScaleOrLength());
    }
}