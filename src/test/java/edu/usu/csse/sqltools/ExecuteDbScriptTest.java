package edu.usu.csse.sqltools;

import org.postgresql.util.PSQLException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


public class ExecuteDbScriptTest {

    private Connection conn;

    @BeforeEach
    public void setUp() throws Exception {
        this.conn = TestingUtils.getConnection();
        int count = ExecuteDbScript.executeFromResource(
                conn, "sqlScripts/ExecuteDbScriptTest/createTestSchema.sql", null);
        assertEquals(0, count);
    }

    @Test
    public void testExecuteWithoutParameters() throws Exception {
        String command = "INSERT INTO db_script_test.person" +
                        " (person_id, first_name, last_name)\n" +
                        " VALUES ('1', 'Joe','Jones');";
        int count = ExecuteDbScript.execute(conn, command);
        assertEquals(1, count);

        command = "INSERT INTO db_script_test.person" +
                " (person_id, first_name, last_name)\n" +
                " VALUES ('2', 'Sue','Smith');";
        count = ExecuteDbScript.execute(conn, command);
        assertEquals(1, count);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                        " FROM db_script_test.person\n");
        assertTrue(resultSet.next());
        assertEquals(1, resultSet.getInt(1));
        assertEquals("Joe", resultSet.getString(2));
        assertEquals("Jones", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(2, resultSet.getInt(1));
        assertEquals("Sue", resultSet.getString(2));
        assertEquals("Smith", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void testExecuteWithParameters() throws Exception {
        ExecuteDbScript.execute(conn, "DELETE FROM db_script_test.person WHERE person_id=3");

        String command = "INSERT INTO db_script_test.person" +
                " (person_id, first_name, last_name)\n" +
                " VALUES (@id, @firstName, @lastName);";

        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", 3));
        parameters.add(new Parameter("firstName", "Henry"));
        parameters.add(new Parameter("lastName", "Homer"));
        int count = ExecuteDbScript.execute(conn, command, parameters);
        assertEquals(1, count);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_script_test.person\n" +
                " WHERE person_id=3");
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Henry", resultSet.getString(2));
        assertEquals("Homer", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromResource() throws Exception {
        ExecuteDbScript.execute(conn, "DELETE FROM db_script_test.person WHERE person_id>=100 AND person_id<200");
        int count = ExecuteDbScript.executeFromResource(conn,
                "sqlScripts/ExecuteDbScriptTest/insertWithoutParameters-1.sql",
                null);
        assertTrue(count>=0);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_script_test.person\n" +
                " WHERE person_id>=100 AND person_id<200\n" +
                " ORDER BY person_id");
        assertTrue(resultSet.next());
        assertEquals(100, resultSet.getInt(1));
        assertTrue(resultSet.next());
        assertEquals(101, resultSet.getInt(1));
        assertTrue(resultSet.next());
        assertEquals(102, resultSet.getInt(1));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromResourceWithParameters() throws Exception {
        ExecuteDbScript.execute(conn, "DELETE FROM db_script_test.person WHERE person_id=4");
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", 4));
        parameters.add(new Parameter("firstName", "Sally"));
        parameters.add(new Parameter("lastName", "Sanders"));
        int count = ExecuteDbScript.executeFromResource(conn,
                "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                parameters);
        assertTrue(count>=0);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_script_test.person\n" +
                " WHERE person_id=4");
        assertTrue(resultSet.next());
        assertEquals(4, resultSet.getInt(1));
        assertEquals("Sally", resultSet.getString(2));
        assertEquals("Sanders", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromFile() throws Exception {
        ExecuteDbScript.execute(conn, "DELETE FROM db_script_test.person WHERE person_id>=200 AND person_id<300");
        int count = ExecuteDbScript.executeFromFile(
                conn,
                "src/test/resources/sqlScripts/ExecuteDbScriptTest/insertWithoutParameters-2.sql",
                null);
        assertTrue(count>=0);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_script_test.person\n" +
                " WHERE person_id>=200 AND person_id<300\n" +
                " ORDER BY person_id");
        assertTrue(resultSet.next());
        assertEquals(200, resultSet.getInt(1));
        assertTrue(resultSet.next());
        assertEquals(201, resultSet.getInt(1));
        assertTrue(resultSet.next());
        assertEquals(202, resultSet.getInt(1));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromFileWithParameters() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", 5));
        parameters.add(new Parameter("firstName", "Timmy"));
        parameters.add(new Parameter("lastName", "Tumbler"));
        int count = ExecuteDbScript.executeFromFile(conn,
                "src/test/resources/sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                parameters);
        assertEquals(1, count);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_script_test.person\n" +
                " WHERE person_id=5");
        assertTrue(resultSet.next());
        assertEquals(5, resultSet.getInt(1));
        assertEquals("Timmy", resultSet.getString(2));
        assertEquals("Tumbler", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void executeFromFileThrowsWhenPathInvalid() throws Exception {
        assertThrows(FileNotFoundException.class, ()-> {
            ExecuteDbScript.executeFromFile(conn, "nonExistentFile.txt", null);
        });
    }

    @Test
    public void connectionIsNull() throws Exception {
        int count = ExecuteDbScript.execute(null, "SELECT 1;", null);
        assertEquals(0, count);

        count = ExecuteDbScript.executeFromResource(null,
                "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                null);
        assertEquals(0, count);

        count = ExecuteDbScript.executeFromFile(null,
                    "src/test/resources/sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    null);
        assertEquals(0, count);
    }

    @Test
    public void scriptResourceIsIncorrect() throws Exception {
        try {
            ExecuteDbScript.executeFromResource(conn, "", null);
            fail("Expected exception not thrown");
        } catch (PSQLException e) {
            //ignore
        }

        int count = ExecuteDbScript.executeFromResource(conn, "sqlScripts/emptyFile.sql", null);
        assertEquals(0, count);

        try {
            ExecuteDbScript.executeFromResource(conn, "anyRandomFile", null);
            fail("Expected exception not thrown");
        } catch (IOException e) {
            // ignore
        }

        try {
            ExecuteDbScript.executeFromResource(conn, "sqlScripts/ExecuteDbScriptTest/incorrectSql.sql", null);
            fail("Expected exception not thrown");
        } catch (PSQLException e) {
            // ignore
        }
    }

    @Test
    public void numberOfParametersGivenAreLessThanExpected() throws Exception {
        List<Parameter> params = new ArrayList<>();
        params.add(new Parameter("firstName", "Linda", Types.VARCHAR));

        assertThrows(PSQLException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    params);
        });
    }

    @Test
    public void numberOfParametersGivenAreMoreThanExpected() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", "6", Types.VARCHAR));
        parameters.add(new Parameter("firstName", "Peter"));
        parameters.add(new Parameter("lastName", "Pepper"));
        parameters.add(new Parameter("extra", "bad"));

        assertThrows(IllegalArgumentException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    parameters);
        });
    }

    @Test
    public void incorrectTypeOfParameter() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("id", "6", Types.VARCHAR));
        parameters.add(new Parameter("firstName", "Peter"));
        parameters.add(new Parameter("lastName", "Pepper"));

        assertThrows(PSQLException.class, ()-> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    parameters);
        });
    }

    @Test
    public void paramsAreEmptyForPreparedStatement() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        assertThrows(PSQLException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    parameters);
        });
    }

    @Test
    public void paramsAreNullForPreparedStatement() throws Exception {
        assertThrows(PSQLException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    null);
        });
    }

    @Test
    public void paramsAreNotNullEvenWhenPreparedStatementIsNotRequired() throws Exception {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(new Parameter("firstName", "Linda", Types.VARCHAR));

        assertThrows(PSQLException.class, () -> {
            ExecuteDbScript.executeFromResource(conn,
                    "sqlScripts/ExecuteDbScriptTest/insertWithParameters.sql",
                    parameters);
        });
    }

    @AfterEach
    public void tearDown() throws Exception {
        ExecuteDbScript.executeFromResource(
                conn,
                "sqlScripts/ExecuteDbScriptTest/dropTestSchema.sql",
                null);
    }
}