package edu.usu.csse.sqltools;

import edu.usu.csse.strings.StringExtension;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class NamedPreparedStatementTest {
    private Connection conn;

    @BeforeEach
    public void setUp() throws Exception {
        this.conn = TestingUtils.getConnection();
    }

    @Test
    public void constructorAndGetter() throws SQLException {
        String query1 = "select * from test.people where first_name=@name;";

        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query1);
        assertNotNull(namedPreparedStatement.getPreparedStatement());
        String expectedQuery = "select * from test.people where first_name=?";
        assertEquals(expectedQuery, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingACorrectQuery() throws SQLException {
        String query = "select * from test.people where first_name=@firstName and last_name=@lastName;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setObject("firstName","Kim");
        namedPreparedStatement.setObject("lastName","Jones");

        expected = "select * from test.people where first_name='Kim' and last_name='Jones'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingWithNoParametersInQuery() throws SQLException {
        String query = "select * from test.people;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);
        String expected = "select * from test.people";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        String query2 = "select * from test.people where first_name='Kim' and last_name='Jones';";
        NamedPreparedStatement namedPreparedStatement2 = new NamedPreparedStatement(conn, query2);
        expected = "select * from test.people where first_name='Kim' and last_name='Jones'";
        assertEquals(expected, namedPreparedStatement2.getPreparedStatement().toString());
    }

    @Test
    public void parsingWithCharacterAtNonParameterPlaces() throws SQLException {
        String query = "select * from test.people where first_name='@Kim' and last_name=@name;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);
        String expected = "select * from test.people where first_name='@Kim' and last_name=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setObject("name","Jones");
        expected = "select * from test.people where first_name='@Kim' and last_name='Jones'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingWhenOtherCharactersAreGiven() throws SQLException {
        String query = "select * from test.people where name=:name and age=$age;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);
        String expected = "select * from test.people where name=:name and age=$age";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        String query2 = "select * from test.people where name=name and age=age;";
        NamedPreparedStatement namedPreparedStatement2 = new NamedPreparedStatement(conn, query2);
        expected = "select * from test.people where name=name and age=age";
        assertEquals(expected, namedPreparedStatement2.getPreparedStatement().toString());
    }

    @Test
    public void setObjectWhenOtherCharactersAreGiven() throws SQLException {
        String query = "select * from test.people where first_name=:firstName and last_name=$lastName;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        assertThrows(IllegalArgumentException.class, () -> {
            namedPreparedStatement.setObject("firstName", "Kim");
            namedPreparedStatement.setObject("lastName", "Jo");
        });

        String expected = "select * from test.people where first_name=:firstName and last_name=$lastName";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingWithSameNamedParameters() throws SQLException {
        String query = "select * from test.people where first_name=@name and last_name=@name;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setObject("name", "Jo");
        expected = "select * from test.people where first_name='Jo' and last_name='Jo'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingWithEmptyQuery() throws Exception {
        String query = "";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);
        assertNotNull(namedPreparedStatement.getPreparedStatement());
        assertEquals("", namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingWithNullQuery() throws SQLException {
        assertThrows(SQLException.class, () -> {
            new NamedPreparedStatement(conn, null);
        });
    }

    @Test
    public void nullConnection() throws SQLException {
        String query = "select * from test.people where name=@name;";
        assertThrows(SQLException.class, () -> {
            new NamedPreparedStatement(null, query);
        });
    }

    @Test
    public void settingTheParameterWhenNoneExist() throws SQLException {
        String query = "select * from test.people where first_name='Kim' and last_name='Jones';";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name='Kim' and last_name='Jones'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        assertThrows(IllegalArgumentException.class, () -> {
            namedPreparedStatement.setObject("name", "Kim");
        });
    }

    @Test
    public void settingWithIncorrectParameter() throws SQLException {
        String query = "select * from test.people where first_name=@firstName and last_name='Jones';";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name='Jones'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        assertThrows(IllegalArgumentException.class, () -> {
            namedPreparedStatement.setObject("name", "Kim");
        });

        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void parsingWithDifferentValuesOfSameParameter() throws SQLException {
        String query = "select * from test.people where first_name=@name and last_name='Jones';";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name='Jones'";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);

        namedPreparedStatement.setObject("name", "Kim");
        namedPreparedStatement.setObject("name", "Jones");

        expected = "select * from test.people where first_name='Jones' and last_name='Jones'";
        actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void parsingWithDifferentCharactersInParameterName() throws SQLException {
        String query = "select * from test.people where first_name=@first_name;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
        namedPreparedStatement.setObject("first_name", "Kim");

        expected = "select * from test.people where first_name='Kim'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        String query2 = "select * from test.people where first_name=@first.name;";
        NamedPreparedStatement namedPreparedStatement2 = new NamedPreparedStatement(conn, query2);
        expected = "select * from test.people where first_name=?";
        assertNotEquals(expected, namedPreparedStatement2.getPreparedStatement().toString());

        String query3 = "select * from test.people where first_name=@first-name;";
        NamedPreparedStatement namedPreparedStatement3 = new NamedPreparedStatement(conn, query3);
        expected = "select * from test.people where first_name=?";
        assertNotEquals(expected, namedPreparedStatement3.getPreparedStatement().toString());
    }

    @Test
    public void parsesAndExecutesQueriesFromAFile() throws Exception {
        String newLineChar = System.lineSeparator();

        String sqlCommands = StringExtension.readStringFromResource("sqlScripts/namedParameterQueries.sql");

        NamedPreparedStatement statement = new NamedPreparedStatement(conn, sqlCommands);
        statement.setObject("firstName1", "Shauna");
        statement.setObject("lastName1", "Thomson");
        statement.setObject("lastName2", "Taylor");
        statement.setObject("age2", 26);

        String expected = "DROP SCHEMA IF EXISTS test CASCADE;" + newLineChar +
                newLineChar +
                "CREATE SCHEMA test;" + newLineChar +
                newLineChar +
                "CREATE TABLE test.people (" + newLineChar +
                "  person_id NUMERIC(12) PRIMARY KEY NOT NULL," + newLineChar +
                "  first_name VARCHAR(40)," + newLineChar +
                "  last_name VARCHAR(40)," + newLineChar +
                "  age INTEGER" + newLineChar +
                ");" + newLineChar +
                newLineChar +
                "-- Shauna Thomson" + newLineChar +
                "INSERT INTO test.people (person_id," + newLineChar +
                "                      first_name, last_name, age)" + newLineChar +
                "          VALUES     ('18334261'," + newLineChar +
                "                      ?, ?, 43);" + newLineChar +
                newLineChar +
                "-- Kay Taylor" + newLineChar +
                "INSERT INTO test.people (person_id," + newLineChar +
                "                      first_name, last_name, age)" + newLineChar +
                "          VALUES     ('18338662'," + newLineChar +
                "                      'Kay', ?, ?)";

        assertEquals(expected, statement.getPreparedStatement().toString());
        statement.execute();

        Statement statement1 = conn.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        String query = "select * from test.people;";
        ResultSet resultSet = statement1.executeQuery(query);
        assertNotNull(resultSet);
        assertTrue(resultSet.next());
        assertEquals("Shauna", resultSet.getString(2));
        assertEquals("Thomson", resultSet.getString(3));
        assertEquals(43, resultSet.getInt(4));

        assertTrue(resultSet.next());
        assertEquals("Kay", resultSet.getString(2));
        assertEquals("Taylor", resultSet.getString(3));
        assertEquals(26, resultSet.getInt(4));

        assertTrue(resultSet.last());
        assertEquals(2, resultSet.getRow());
    }

    @Test
    public void checksExecuteQuery() throws Exception {
        String newLineChar = System.lineSeparator();

        Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        String query = "DROP SCHEMA IF EXISTS test CASCADE;" + newLineChar +
                "CREATE SCHEMA test;" + newLineChar +
                "CREATE TABLE test.people (" + newLineChar +
                "  person_id NUMERIC(12) PRIMARY KEY NOT NULL," + newLineChar +
                "  first_name VARCHAR(40));";

        statement.execute(query);

        statement.execute("Insert into test.people (person_id, first_name) values ('18338662', 'Shauna');");
        statement.execute("Insert into test.people (person_id, first_name) values ('18338663', 'Kay');");

        String query1 = "select * from test.people where first_name=@name;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query1);
        namedPreparedStatement.setString("name", "Kay");

        ResultSet resultSet = namedPreparedStatement.executeQuery();
        assertNotNull(resultSet);
        assertTrue(resultSet.next());
        assertEquals("Kay", resultSet.getString(2));
        assertTrue(resultSet.last());
        assertEquals(1, resultSet.getRow());
    }

    @Test
    public void shouldNotConsiderVariablesInComments() throws Exception {
        String newLineChar = System.lineSeparator();

        String sqlCommands = StringExtension.readStringFromResource("sqlScripts/namedQueriesWithParametersInComments.sql");

        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, sqlCommands);
        namedPreparedStatement.setObject("firstName1", "Shauna");
        namedPreparedStatement.setObject("lastName1", "Thomson");

        String expected = "DROP SCHEMA IF EXISTS test CASCADE;" + newLineChar +
                newLineChar +
                "CREATE SCHEMA test;" + newLineChar +
                newLineChar +
                "CREATE TABLE test.people (" + newLineChar +
                "  person_id NUMERIC(12) PRIMARY KEY NOT NULL," + newLineChar +
                "  first_name VARCHAR(40)," + newLineChar +
                "  last_name VARCHAR(40)," + newLineChar +
                "  age INTEGER" + newLineChar +
                ");" + newLineChar +
                newLineChar +
                "-- Shauna Thomson @shouldNotConsiderThisAsParameter" + newLineChar +
                "INSERT INTO test.people (person_id," + newLineChar +
                "                      first_name, last_name, age)" + newLineChar +
                "          VALUES     ('18334261', --@shouldNotConsiderThisAsParameter" + newLineChar +
                "                      ?, ?, 43)";

        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
        int count = namedPreparedStatement.execute();
        assertEquals(0, count);

        Statement statement = conn.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        String query = "select * from test.people;";
        ResultSet resultSet = statement.executeQuery(query);
        assertNotNull(resultSet);
        assertTrue(resultSet.next());
        assertEquals("Shauna", resultSet.getString(2));
        assertEquals("Thomson", resultSet.getString(3));
        assertEquals(43, resultSet.getInt(4));

        assertTrue(resultSet.last());
        assertEquals(1, resultSet.getRow());
    }

    @Test
    public void shouldIgnoreCommentInASingleLineQuery() throws SQLException {
        String query = "select * from test.people where first_name=@firstName --and last_name=@lastName;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? --and last_name=@lastName;";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);

        namedPreparedStatement.setObject("firstName","Kim");
        expected = "select * from test.people where first_name='Kim' --and last_name=@lastName;";
        actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void shouldIgnoreCommentInQuotes() throws SQLException {
        String query = "select * from test.people where first_name=\"Kim--J\" and last_name=@lastName;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=\"Kim--J\" and last_name=?";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);

        namedPreparedStatement.setObject("lastName","Jones");

        expected = "select * from test.people where first_name=\"Kim--J\" and last_name='Jones'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());


        String query2 = "select * from test.people where first_name='Kim--J' and last_name=@lastName;";
        NamedPreparedStatement statement = new NamedPreparedStatement(conn, query2);

        String expected2 = "select * from test.people where first_name='Kim--J' and last_name=?";
        assertEquals(expected2, statement.getPreparedStatement().toString());

        statement.setObject("lastName","Jones");

        expected2 = "select * from test.people where first_name='Kim--J' and last_name='Jones'";
        assertEquals(expected2, statement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetObjectInStatement() throws SQLException {
        String query = "select * from test.people where first_name=@name and points=@points and rate=@rate " +
                "and percent=@percent and phones=@phones;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and points=? and rate=? and " +
                "percent=? and phones=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setObject("name","Kim");
        namedPreparedStatement.setObject("points",3.4146, Types.DECIMAL);
        namedPreparedStatement.setObject("rate",43.1234, Types.DECIMAL, 4);
        namedPreparedStatement.setObject("percent",75.1234567, Types.DECIMAL, 5);
        Integer[] phonesArray = {123456, 654321};
        namedPreparedStatement.setObject("phones",
                conn.createArrayOf("Integer", phonesArray), Types.ARRAY);

        expected = "select * from test.people where first_name='Kim' and points=3.4146 and rate=43.1234 and " +
                "percent=75.12346 and phones=?";
        // Note: the object parameter doesn't seem to be converted in toString()
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void setObjectShouldSetNullValues() throws SQLException {
        String query = "select * from test.people where first_name=@name and age=@age and married=@married " +
                "and dob=@dob and time=@time;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and age=? and married=? " +
                "and dob=? and time=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setObject("name",null, Types.VARCHAR);
        namedPreparedStatement.setObject("age",null, Types.INTEGER);
        namedPreparedStatement.setObject("married",null, Types.BOOLEAN);
        namedPreparedStatement.setObject("dob",null, Types.DATE);
        namedPreparedStatement.setObject("time",null, Types.TIME);

        expected = "select * from test.people where first_name=NULL and age=NULL and married=NULL " +
                "and dob=NULL and time=NULL";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetString() throws SQLException {
        String query = "select * from test.people where first_name=@name and last_name=@lastName and gender=@gender;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name=? and gender=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setString("name","Kim");
        namedPreparedStatement.setString("lastName","Jones");
        namedPreparedStatement.setString("gender", "M");

        expected = "select * from test.people where first_name='Kim' and last_name='Jones' and gender='M'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setString("name",null);
        namedPreparedStatement.setString("lastName",null);
        namedPreparedStatement.setString("gender", null);

        expected = "select * from test.people where first_name=NULL and last_name=NULL and gender=NULL";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetStringWithTrim() throws SQLException {
        String query = "select * from test.people where first_name=@name and last_name=@lastName and gender=@gender;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name=? and gender=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setString("name"," Kim ", true, false);
        namedPreparedStatement.setString("lastName"," Jones", true, false);
        namedPreparedStatement.setString("gender", "M   \n", true, false);

        expected = "select * from test.people where first_name='Kim' and last_name='Jones' and gender='M'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetStringWithUpperCase() throws SQLException {
        String query = "select * from test.people where first_name=@name and last_name=@lastName and gender=@gender;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where first_name=? and last_name=? and gender=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setString("name","Kim", false, true);
        namedPreparedStatement.setString("lastName","Jones", false, true);
        namedPreparedStatement.setString("gender", "M", false, true);

        expected = "select * from test.people where first_name='KIM' and last_name='JONES' and gender='M'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetShortIntLongDoubleFloatDataTypes() throws SQLException {
        String query = "select * from test.people where num_children=@child and num_jobs=@job " +
                "and age=@age1 and points=@points1 and percent=@per1 and rate=@rate1;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where num_children=? and num_jobs=? and age=? and points=? " +
                "and percent=? and rate=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setShort("child", (short) 2);
        namedPreparedStatement.setInt("job",1);
        namedPreparedStatement.setInt("age1",30);
        namedPreparedStatement.setLong("points1",123456789L);
        namedPreparedStatement.setDouble("per1",56.2134);
        namedPreparedStatement.setFloat("rate1", (float) 56.2);

        expected = "select * from test.people where num_children=2 and num_jobs=1 and age=30 and " +
                "points=123456789 and percent=56.2134 and rate=56.2";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setShort("child", null);
        namedPreparedStatement.setInt("job",null);
        namedPreparedStatement.setInt("age1",null);
        namedPreparedStatement.setLong("points1",null);
        namedPreparedStatement.setDouble("per1",null);
        namedPreparedStatement.setFloat("rate1", null);

        expected = "select * from test.people where num_children=NULL and num_jobs=NULL and age=NULL and points=NULL " +
                "and percent=NULL and rate=NULL";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetBigDecimal() throws SQLException {
        String query = "select * from test.people where transaction_id=@id and report_num=@num;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where transaction_id=? and report_num=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setBigDecimal("id", new java.math.BigDecimal(1234));
        namedPreparedStatement.setBigDecimal("num", new java.math.BigDecimal(5678));

        expected = "select * from test.people where transaction_id=1234 and report_num=5678";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

    }

    @Test
    public void shouldSetBoolean() throws SQLException {
        String query = "select * from test.people where married=@married and employed=@employed;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where married=? and employed=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setBoolean("married",true);
        namedPreparedStatement.setBoolean("employed",false);

        expected = "select * from test.people where married='TRUE' and employed='FALSE'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setBoolean("married",null);
        namedPreparedStatement.setBoolean("employed",null);

        expected = "select * from test.people where married=NULL and employed=NULL";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void shouldSetDateTimeTimestamp() throws SQLException {
        String query = "select * from test.people where birth_date=@bd and birth_time=@bt and member_since=@mem " +
                "and last_transaction=@trans;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where birth_date=? and birth_time=? and member_since=? " +
                "and last_transaction=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        Date birthDate = Date.valueOf("1985-03-21");
        Time birthTime = new Time(1521684166466L);
        Date memberSince = Date.valueOf("2010-06-18");
        Timestamp timestamp = Timestamp.valueOf("1985-03-21 20:14:47");

        namedPreparedStatement.setDate("bd", birthDate);
        namedPreparedStatement.setTime("bt", birthTime);
        namedPreparedStatement.setDate("mem", memberSince);
        namedPreparedStatement.setTimestamp("trans", timestamp);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd X");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:s.SSSX");
        SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd H:mm:sX");

        expected = "select * from test.people where birth_date='" +
                dateFormat.format(birthDate).replace("Z", "+00") +
                "' and birth_time='" + timeFormat.format(birthTime).replace("Z", "+00") +
                "' and member_since='" + dateFormat.format(memberSince).replace("Z", "+00") +
                "' and last_transaction='" + timestampFormat.format(timestamp).replace("Z", "+00") + "'";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setDate("bd", null);
        namedPreparedStatement.setTime("bt", null);
        namedPreparedStatement.setDate("mem", null);
        namedPreparedStatement.setTimestamp("trans", null);

        expected = "select * from test.people where birth_date=NULL" +
                " and birth_time=NULL" +
                " and member_since=NULL" +
                " and last_transaction=NULL";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

    }

    @Test
    public void shouldSetNull() throws SQLException {
        String query = "select * from test.people where name=@name and age=@age and employed=@employed and dob=@dob;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select * from test.people where name=? and age=? and employed=? and dob=?";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());

        namedPreparedStatement.setNull("name",Types.VARCHAR);
        namedPreparedStatement.setNull("age",Types.INTEGER);
        namedPreparedStatement.setNull("employed",Types.BOOLEAN);
        namedPreparedStatement.setNull("dob",Types.DATE);

        expected = "select * from test.people where name=NULL and age=NULL and employed=NULL and dob=NULL";
        assertEquals(expected, namedPreparedStatement.getPreparedStatement().toString());
    }

    @Test
    public void testCommandsWithStdIn() throws SQLException {
        String query = "COPY public.adoption_status (code, description) FROM stdin;\n" +
                "C\tComplete\n" +
                "P\tPending\n" +
                "\\. -- This is a test block copy command";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "COPY public.adoption_status (code, description) FROM stdin;\n" +
                "C\tComplete\n" +
                "P\tPending\n" +
                "\\. -- This is a test block copy command";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testCommandsWithStdInAndNoEndMarker() throws SQLException {
        String query = "COPY public.adoption_status (code, description) FROM stdin;\n" +
                "C\tComplete\n" +
                "P\tPending\n";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "COPY public.adoption_status (code, description) FROM stdin;\n" +
                "C\tComplete\n" +
                "P\tPending\n";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testCommandsWithStdInAndAtSigns() throws SQLException {
        String query = "COPY public.adoption_status (code, description) FROM stdin;\n" +
                "@\t@Complete\n" +
                "C\tComplete\n" +
                "P\tPending\n";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "COPY public.adoption_status (code, description) FROM stdin;\n" +
                "@\t@Complete\n" +
                "C\tComplete\n" +
                "P\tPending\n";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testInvalidParamNames() throws SQLException {
        String query = "select @12;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select @12";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testNullQuery() throws SQLException {
        assertThrows(SQLException.class, () -> {
            new NamedPreparedStatement(conn, null);
        });
    }

    @Test
    public void testParseNullQuery() throws SQLException {
        Map<String, List<Integer>> map = new HashMap<>();
        String result = NamedPreparedStatement.parse(null, map);
        assertEquals("", result);
    }


    @Test
    public void testParseQueryWithUnmatchedQuote() throws SQLException {
        Map<String, List<Integer>> map = new HashMap<>();
        try {
            String query = "select 'test', 'testing;";
            NamedPreparedStatement.parse(query, map);
            fail("Expected exception not thrown");
        } catch (SQLException err) {
            assertEquals("Query contains an unmatched quote", err.getMessage());
        }

    }

    @Test
    public void testMultiStatements() throws SQLException {
        String query = "select @test1; select @test2; select @test3;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);

        String expected = "select ?; select ?; select ?";
        String actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);

        namedPreparedStatement.setString("test1","test1");
        namedPreparedStatement.setString("test2","test2");
        namedPreparedStatement.setString("test2","test2");

        // NOTE: Substitution does not work when there are multiple statements
        expected = "select ?; select ?; select ?";
        actual = namedPreparedStatement.getPreparedStatement().toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testContainsParameters() throws SQLException {
        String query = "select @test1; select @test2; select @test3;";
        NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, query);
        assertTrue(namedPreparedStatement.containsParameter("test1"));
        assertTrue(namedPreparedStatement.containsParameter("test2"));
        assertTrue(namedPreparedStatement.containsParameter("test3"));
        assertFalse(namedPreparedStatement.containsParameter("test4"));
        assertFalse(namedPreparedStatement.containsParameter(""));
        assertFalse(namedPreparedStatement.containsParameter(null));
    }

}