package edu.usu.csse.sqltools;

import edu.usu.csse.properties.PropertyLoader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class TestingUtils {
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Properties testProperties = PropertyLoader.createPropertiesFromResource("connection.properties");

        Class.forName(testProperties.getProperty("db-driver"));
        return DriverManager.getConnection(testProperties.getProperty("db-connection"),
                testProperties.getProperty("db-username"),
                testProperties.getProperty("db-password"));
    }
}
