package edu.usu.csse.sqltools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("SqlResolve")
public class PreparedStatementSetTest {
    private Connection conn;

    @BeforeEach
    public void setUp() throws Exception {
        this.conn = TestingUtils.getConnection();
        int count = ExecuteDbScript.executeFromResource(
                conn, "sqlScripts/PreparedStatementSetTest/createTestSchema.sql", null);
        assertEquals(0, count);
    }

    @Test
    public void testAddWithNull() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();
        assertFalse(statements.add(null));
    }

    @Test
    public void testAddStatements() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();

        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                    "            VALUES (5, 'John', 'Johnson');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (6, 'Gary', 'Garrison');");
        assertTrue(statements.add(statement));
    }

    @Test
    public void testAddWithDifferentConnection() throws SQLException, ClassNotFoundException {
        PreparedStatementSet statements = new PreparedStatementSet();

        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (5, 'John', 'Johnson');");
        assertTrue(statements.add(statement));

        Connection altConn = TestingUtils.getConnection();

        try {
            statement = altConn.prepareStatement(
                    "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                            "            VALUES (6, 'Gary', 'Garrison');");
            statements.add(statement);
            fail("Expected exception not thrown");
        }
        catch (SQLException ex) {
            assertEquals("The statement's connection is not the same previous statements", ex.getMessage());
        }
    }

    @Test
    public void testExecute() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();

        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (5, 'John', 'Johnson');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (6, 'Gary', 'Garrison');");
        assertTrue(statements.add(statement));

        boolean result = statements.execute();
        assertTrue(result);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_prepared_statement_test.person WHERE person_id<=6 ORDER BY person_id\n");
        assertTrue(resultSet.next());
        assertEquals(1, resultSet.getInt(1));
        assertEquals("Linda", resultSet.getString(2));
        assertEquals("Larson", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(2, resultSet.getInt(1));
        assertEquals("Kim", resultSet.getString(2));
        assertEquals("Jones", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(3, resultSet.getInt(1));
        assertEquals("Jimmy", resultSet.getString(2));
        assertEquals("Johns", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(4, resultSet.getInt(1));
        assertEquals("Ivan", resultSet.getString(2));
        assertEquals("Iceland", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(5, resultSet.getInt(1));
        assertEquals("John", resultSet.getString(2));
        assertEquals("Johnson", resultSet.getString(3));
        assertTrue(resultSet.next());
        assertEquals(6, resultSet.getInt(1));
        assertEquals("Gary", resultSet.getString(2));
        assertEquals("Garrison", resultSet.getString(3));
        assertFalse(resultSet.next());
    }

    @Test
    public void testExecuteWithNoStatements() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();
        assertFalse(statements.execute());
    }

    @Test
    public void testExecuteAtomically() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();
        conn.setAutoCommit(true);

        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (7, 'Mary', 'Matthews');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (8, 'Nancy', 'Nom');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (9, 'Paul', 'Pearson');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (10, 'Robert', 'Robson');");
        assertTrue(statements.add(statement));

        boolean result = statements.executeAtomically();
        assertTrue(result);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_prepared_statement_test.person WHERE person_id>=7 AND person_id<=10 ORDER BY person_id\n");
        for (int i=7; i<=10; i++) {
            assertTrue(resultSet.next());
            assertEquals(i, resultSet.getInt(1));
        }
        assertFalse(resultSet.next());

        assertTrue(conn.getAutoCommit());
    }

    @Test
    public void testExecuteAtomicallyWithNoStatements() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();
        assertFalse(statements.executeAtomically());
    }

    @Test
    public void testExecuteAtomicallyFailure() throws SQLException {
        PreparedStatementSet statements = new PreparedStatementSet();

        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (12, 'Dan', 'Daniels');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (13, 'Eric', 'Earl');");
        assertTrue(statements.add(statement));

        statement = conn.prepareStatement(
                "INSERT INTO db_prepared_statement_test.person (person_id, first_name, last_name)\n" +
                        "            VALUES (13, 'Eric', 'Earl');");
        assertTrue(statements.add(statement));

        boolean result = statements.executeAtomically();
        assertFalse(result);

        ResultSet resultSet = ExecuteDbQuery.execute(conn, "SELECT person_id, first_name, last_name\n" +
                " FROM db_prepared_statement_test.person WHERE person_id>=12 AND person_id<=13 ORDER BY person_id\n");
        assertFalse(resultSet.next());
    }

    @AfterEach
    public void tearDown() throws Exception {
        ExecuteDbScript.executeFromResource(
                conn,
                "sqlScripts/PreparedStatementSetTest/dropTestSchema.sql",
                null);
    }
}