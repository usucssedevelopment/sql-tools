package edu.usu.csse.sqltools;

import java.sql.Types;

/**
 * Parameter
 *
 * Objects of this class are used to make substitutions in named parameters in NamedPreparedStatements.
 */
@SuppressWarnings("WeakerAccess")
public class Parameter {
    private String parameterName;
    private int parameterIndex;
    private int targetSqlType;
    private Object value;
    private int scaleOrLength;

    /**
     * Create a parameter of with a varchar type
     *
     * @param parameterName     Parameter Name
     * @param value             Parameter Value
     */
    public Parameter(String parameterName, String value) {
        this.parameterName = parameterName;
        this.value = value;
        this.targetSqlType = Types.VARCHAR;
    }

    /**
     * Create a parameter of with a integer type
     *
     * @param parameterName     Parameter Name
     * @param value             Parameter Value
     */
    public Parameter(String parameterName, int value) {
        this.parameterName = parameterName;
        this.value = value;
        this.targetSqlType = Types.INTEGER;
    }

    /**
     * Create a parameter of some specified data type
     *
     * @param parameterName     Parameter Name
     * @param value             Parameter Value
     * @param targetSqlType     An instance of Types enumeration that specifies the desired data type
     */
    public Parameter(String parameterName, Object value, int targetSqlType) {
        this.parameterName = parameterName;
        this.value = value;
        this.targetSqlType = targetSqlType;
    }

    /**
     * Create a parameter of some specified data type and some specified scale or length
     *
     * @param parameterName     Parameter Name
     * @param value             Parameter Value
     * @param targetSqlType     An instance of the java.sql.Types enumeration that specifies the desired data type
     * @param scaleOrLength     The desired scale or length
     */
    public Parameter(String parameterName, Object value, int targetSqlType, int scaleOrLength) {
        this.parameterName = parameterName;
        this.value = value;
        this.targetSqlType = targetSqlType;
        this.scaleOrLength = scaleOrLength;
    }

    /**
     * Create a parameter of with a varchar type
     *
     * @param parameterIndex    Parameter Index
     * @param value             Parameter Value
     */
    public Parameter(int parameterIndex, String value){
        this.parameterIndex = parameterIndex;
        this.value = value;
        this.targetSqlType = Types.VARCHAR;
    }

    /**
     * Create a parameter of with a integer type
     *
     * @param parameterIndex    Parameter Index
     * @param value             Parameter Value
     */
    public Parameter(int parameterIndex, int value){
        this.parameterIndex = parameterIndex;
        this.value = value;
        this.targetSqlType = Types.INTEGER;
    }

    /**
     * Create a parameter of some specified data type
     *
     * @param parameterIndex    Parameter Index
     * @param value             Parameter Value
     * @param targetSqlType     An instance of the java.sql.Types enumeration that specifies the desired data type
     */
    public Parameter(int parameterIndex, Object value, int targetSqlType) {
        this.parameterIndex = parameterIndex;
        this.value = value;
        this.targetSqlType = targetSqlType;
    }

    /**
     * Create a parameter of some specified data type and some specified scale or length
     *
     * @param parameterIndex    Parameter Index
     * @param value             Parameter Value
     * @param targetSqlType     An instance of the java.sql.Types enumeration that specifies the desired data type
     * @param scaleOrLength     The desired scale or length
     */
    public Parameter(int parameterIndex, Object value, int targetSqlType, int scaleOrLength) {
        this.parameterIndex = parameterIndex;
        this.value = value;
        this.targetSqlType = targetSqlType;
        this.scaleOrLength = scaleOrLength;
    }

    /**
     * @return                  The parameter's name
     */
    public String getParameterName() {
        return parameterName;
    }

    /**
     * Set the parameter's name
     *
     * @param parameterName     Parameter Name
     */
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    /**
     * @return                  The parameter's index
     */
    public int getParameterIndex() {
        return parameterIndex;
    }

    /**
     * @param parameterIndex    Set the parameter's index
     */
    public void setParameterIndex(int parameterIndex) {
        this.parameterIndex = parameterIndex;
    }

    /**
     * @return                  The parameter's SQL Type (as an integer)
     */
    public int getTargetSqlType() {
        return targetSqlType;
    }

    /**
     * Set the parameter's SQL type.
     *
     * @param targetSqlType     Set the parameters's SQL Type.   Should be an instance of the java.sql.Types
     *                          enumeration.
     */
    public void setTargetSqlType(int targetSqlType) {
        this.targetSqlType = targetSqlType;
    }

    /**
     * @return                  The parameter's value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Set the parameter's value
     *
     * @param value             The parameter's value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * @return                  The parameter's scale or length
     */
    public int getScaleOrLength() {
        return scaleOrLength;
    }

    /**
     * Set the parameter's scale or length
     *
     * @param scaleOrLength     A scale or length, depending on the parameter's type
     */
    public void setScaleOrLength(int scaleOrLength) {
        this.scaleOrLength = scaleOrLength;
    }
}
