package edu.usu.csse.sqltools;

import edu.usu.csse.strings.StringExtension;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class ExecuteDbQuery {
    private static Logger log = LogManager.getLogger(ExecuteDbQuery.class.getName());

    /**
     * Executes the SQL query represented in a string, after doing parameter substitution.  That parameters
     * in the string should be named parameters, i.e. an "@" followed by a name.
     *
     * @param conn              The database connection.
     * @param sqlQuery          A string containing the command to execute.  May contain named parameters
     * @param params            A list of Parameter object, with at least one for each named parameter in command
     *                          string.
     * @return                  A ResultSet if the query is not empty and it executed without an exception. Null if
     *                          The connection wasn't specified or the query was empty.
     * @throws SQLException     Thrown an error okay.
     */
    public static ResultSet execute(Connection conn, String sqlQuery, List<Parameter> params)
            throws SQLException {
        if (conn == null || StringExtension.isEmptyOrWhitespace(sqlQuery))
            return null;

        ResultSet resultSet;
        if (params != null && !params.isEmpty()) {
            NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, sqlQuery);
            for (Parameter param : params) {
                namedPreparedStatement.setObject(param.getParameterName(), param.getValue(), param.getTargetSqlType(),
                        param.getScaleOrLength());
            }
            log.trace("Execute {}", namedPreparedStatement.toString());
            resultSet = namedPreparedStatement.executeQuery();
        } else {
            Statement statement = conn.createStatement();
            log.trace("Execute {}", sqlQuery);
            resultSet = statement.executeQuery(sqlQuery);
        }
        return resultSet;
    }

    /**
     * Execute the SQL commands represented in a string.  This simple method does not substitute parameters.
     * If you need to substitute parameters, use the execute method that takes a list of parameters.
     *
     * @param conn              The database connection
     * @param sqlQuery          A string containing the query to execute
     * @return                  A ResultSet if the query is not empty and it executed without an exception.
     * @throws SQLException     Thrown a SQL error occurred
     */
    public static ResultSet execute(Connection conn, String sqlQuery) throws SQLException {
        return execute(conn, sqlQuery, null);
    }

    /**
     * Executes the SQL query contained in a file, after doing parameter substitution.  That parameters
     * in the string should be named parameters, i.e. an "@" followed by a name.
     *
     * @param conn              The database connection.
     * @param scriptFile        The name of a file containing SQL query to execute.  The file may contain
     *                          named parameters.
     * @param params            A list of Parameter objects, with at least one for each named parameter in
     *                          the file.
     * @return                  True if the statement is not empty and it executed without an exception.
     * @throws SQLException     Thrown a SQL error occurred
     * @throws IOException      Thrown an IOException occurred
     */
    public static ResultSet executeFromFile(Connection conn, String scriptFile, List<Parameter> params)
            throws SQLException, IOException {
        String sqlQuery = StringExtension.readStringFromFile(scriptFile);
        return execute(conn, sqlQuery, params);
    }

    /**
     * Executes the SQL query in a resource, after doing parameter substitution.  That parameters
     * in the string should be named parameters, i.e. an "@" followed by a name.
     *
     * @param conn              The database connection.
     * @param scriptResource    The path of a resource containing the query to execute.  The resource may
     *                          contain named parameters.
     * @param params            A list of Parameter objects, with at least one for each named parameter in the
     *                          resource.
     * @return                  True if the statement is not empty and it executed without an exception.
     * @throws SQLException     Thrown a SQL error occurred
     * @throws IOException      Thrown an IOException occurred
     */
    public static ResultSet executeFromResource(Connection conn, String scriptResource, List<Parameter> params)
            throws SQLException, IOException {
        String sqlQuery = StringExtension.readStringFromResource(scriptResource);
        return execute(conn, sqlQuery, params);
    }
}
