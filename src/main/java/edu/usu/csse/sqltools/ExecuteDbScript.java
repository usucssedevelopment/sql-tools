package edu.usu.csse.sqltools;

import edu.usu.csse.strings.StringExtension;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class ExecuteDbScript {
    private static Logger log = LogManager.getLogger(ExecuteDbScript.class.getName());

    /**
     * Executes the SQL commands represented in a string, after doing parameter substitution.  That parameters
     * in the string should be named parameters, i.e. an "@" followed by a name.
     *
     * @param conn              The database connection.
     * @param sqlCommands       A string containing the command to execute.  May contain named parameters
     * @param params            A list of Parameter object, with at least one for each named parameter in command
     *                          string.
     * @return                  The number of effected records for the first command.
     * @throws SQLException     Thrown an error okay.
     */
    public static int execute(Connection conn, String sqlCommands, List<Parameter> params)
            throws SQLException {
        if (conn == null || StringExtension.isEmptyOrWhitespace(sqlCommands))
            return 0;

        boolean isResultSet;
        int result;
        if (params != null && !params.isEmpty()) {
            NamedPreparedStatement namedPreparedStatement = new NamedPreparedStatement(conn, sqlCommands);
            for (Parameter param : params) {
                namedPreparedStatement.setObject(param.getParameterName(), param.getValue(), param.getTargetSqlType(),
                        param.getScaleOrLength());
            }
            log.trace("Execute {}", namedPreparedStatement.toString());
            result = namedPreparedStatement.execute();
        } else {
            Statement statement = conn.createStatement();
            log.trace("Execute {}", sqlCommands);
            isResultSet = statement.execute(sqlCommands);
            result = getResultCount(isResultSet, statement);
        }

        return result;
    }

    /**
     * Execute the SQL commands represented in a string.  This simple method does not substitute parameters.
     * If you need to substitute parameters, use the execute method that takes a list of parameters.
     *
     * @param conn              The database connection
     * @param sqlCommands       A string containing the commands to execute
     * @return                  The number of effected records for the first command.
     * @throws SQLException     Thrown an error okay
     */
    public static int execute(Connection conn, String sqlCommands) throws SQLException {
        return execute(conn, sqlCommands, null);
    }

    /**
     * Executes the SQL commands contained in a file, after doing parameter substitution.  That parameters
     * in the string should be named parameters, i.e. an "@" followed by a name.
     *
     * @param conn              The database connection.
     * @param scriptFile        The name of a file containing SQL commands to execute.  The file may contain
     *                          named parameters.
     * @param params            A list of Parameter objects, with at least one for each named parameter in
     *                          the file.
     * @return                  The number of effected records for the first command.
     * @throws SQLException     Thrown a SQL error occurred
     * @throws IOException      Thrown an IOException occurred
     */
    public static int executeFromFile(Connection conn, String scriptFile, List<Parameter> params)
            throws SQLException, IOException {
        String sqlCommands = StringExtension.readStringFromFile(scriptFile);
        return execute(conn, sqlCommands, params);
    }

    /**
     * Executes the SQL commands in a resource, after doing parameter substitution.  That parameters
     * in the string should be named parameters, i.e. an "@" followed by a name.
     *
     * @param conn              The database connection.
     * @param scriptResource    The path of a resource containing the commands to execute.  The resource may
     *                          contain named parameters.
     * @param params            A list of Parameter objects, with at least one for each named parameter in the
     *                          resource.
     * @return                  The number of effected records for the first command.
     * @throws SQLException     Thrown a SQL error occurred
     * @throws IOException      Thrown an IOException occurred
     */
    public static int executeFromResource(Connection conn, String scriptResource, List<Parameter> params)
            throws SQLException, IOException {
        String sqlCommands = StringExtension.readStringFromResource(scriptResource);
        return execute(conn, sqlCommands, params);
    }

    /**
     * This method can get a count from the results of an execution of a statement
     *
     * @param isResultSet       The boolean results from the call to a statement.execute
     * @param statement         The statement
     * @return                  The count of the records in the result set if the statement was a query, or
     *                          The count of the effected records of the first command in the statement.
     */
    public static int getResultCount(boolean isResultSet, Statement statement) {
        int result;
        try {
            if (isResultSet && statement.getResultSet()!=null) {
                ResultSet rs = statement.getResultSet();
                rs.last();
                result = rs.getRow();
            } else {
                result = statement.getUpdateCount();
            }
        } catch (SQLException e) {
            result = 0;
        }
        return result;
    }
}
