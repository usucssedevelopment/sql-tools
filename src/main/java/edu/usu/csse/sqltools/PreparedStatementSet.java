package edu.usu.csse.sqltools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;

public class PreparedStatementSet {
    private Connection conn;
    private List<PreparedStatement> statementList = new ArrayList<>();

    public boolean add(PreparedStatement statement) throws SQLException {
        if (statement != null) {
            if (conn == null) {
                conn = statement.getConnection();
            } else if (conn != statement.getConnection()) {
                throw new SQLException("The statement's connection is not the same previous statements");
            }
            statementList.add(statement);
            return true;
        }
        return false;
    }

    public boolean executeAtomically() throws SQLException {
        if (conn == null) {
            return false;
        }

        boolean previousAutoCommit = conn.getAutoCommit();
        conn.setAutoCommit(false);

        boolean result;
        try {
            Savepoint savepoint = conn.setSavepoint();

            result = execute();
            if (!result) {
                conn.rollback(savepoint);
            } else {
                try {
                    conn.commit();
                }
                catch(SQLException ex) {
                    conn.rollback(savepoint);
                    result = false;
                }
            }
        }
        finally {
            conn.setAutoCommit(previousAutoCommit);
        }

        return result;
    }

    public boolean execute() throws SQLException {
        if (statementList.size()<1) {
            return false;
        }

        boolean result = true;

        for (PreparedStatement stmt : statementList) {
            try {
                int updateCount = stmt.executeUpdate();
                result = (updateCount >= 0);
            } catch (SQLException e) {
                stmt.close();
                result = false;
            }

            if (!result) {
                break;
            }
        }
        return result;
    }

}
