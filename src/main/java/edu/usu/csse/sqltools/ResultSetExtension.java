package edu.usu.csse.sqltools;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

// TODO: Finish testing

@SuppressWarnings("WeakerAccess")
/*
 * ResultSetExtension
 *
 * This class contains some methods for working with results sets.
 */
public class ResultSetExtension {
    private static final Logger log = LogManager.getLogger(ResultSetExtension.class.getName());

    /**
     * Return a Short Integer field from the current record in a data set.
     *
     * @param dataSet           The dataset
     * @param fieldName         The name of the field to retrieve
     * @return                  The field value, which may be null.  Also, null if there is a problem with
     *                          the dataset or field name.
     */
    public static Short getShortField(ResultSet dataSet, String fieldName) {
        if (dataSet==null || fieldName==null || fieldName.isEmpty()) return null;

        Short value = null;
        try {
            value = dataSet.getShort(fieldName);
            if (dataSet.wasNull()) {
                return null;
            }
        } catch (SQLException e) {
            log.warn("Cannot find field {} in ResultSet", fieldName);
        }
        return value;
    }

    /**
     * Return an Integer field from the current record in a data set.
     *
     * @param dataSet           The dataset
     * @param fieldName         The name of the field to retrieve
     * @return                  The field value, which may be null.  Also, null if there is a problem with
     *                          the dataset or field name.
     */
    public static Integer getIntegerField(ResultSet dataSet, String fieldName) {
        if (dataSet==null || fieldName==null || fieldName.isEmpty()) return null;

        Integer value = null;
        try {
            value = dataSet.getInt(fieldName);
            if (dataSet.wasNull()) {
                return null;
            }
        } catch (SQLException e) {
            log.warn("Cannot find field {} in ResultSet", fieldName);
        }
        return value;
    }

    /**
     * Return a Date field from the current record in a data set.
     *
     * @param dataSet           The dataset
     * @param fieldName         The name of the field to retrieve
     * @return                  The field value, which may be null.  Also, null if there is a problem with
     *                          the dataset or field name.
     */
    public static Date getDateField(ResultSet dataSet, String fieldName) {
        if (dataSet==null || fieldName==null || fieldName.isEmpty()) return null;

        Date value = null;
        try {
            value = dataSet.getDate(fieldName);
            if (dataSet.wasNull()) {
                return null;
            }
        } catch (SQLException e) {
            log.warn("Cannot find field {} in ResultSet", fieldName);
        }
        return value;
    }

    /**
     * Return a field from the current record in a data set with the option of converting to
     * upper case and removing spaces.
     *
     * @param dataSet           The dataset
     * @param fieldName         The name of the field to retrieve
     * @param convertToUpper    If true, then convert the field value to upper case; otherwise, leave as is
     * @param removeSpaces      If true, then remove all spaces from the field value; otherwise, leave as is
     * @return                  The field value.  Null if there is a problem with the dataset or field name.
     */
    public static String getTrimmedStringField(ResultSet dataSet, String fieldName, boolean convertToUpper, boolean removeSpaces) {
        if (dataSet==null || fieldName==null || fieldName.isEmpty()) return null;

        String value = null;
        try {
            value = dataSet.getString(fieldName);
            if (dataSet.wasNull()) {
                return null;
            }
        } catch (SQLException e) {
            log.warn("Cannot find field {} in ResultSet", fieldName);
        }
        if (value == null || value.isEmpty()) {
            return null;
        }

        value = value.trim();
        if (convertToUpper)
            value = value.toUpperCase();

        if (removeSpaces)
            value = value.replaceAll("\\s+","");

        return value;
    }

    /**
     * Return a field that is normally a numeric value as a string
     *
     * @param dataSet       A dataset with a current record
     * @param fieldName     The number of the field to return
     * @return              The value of the field.  Null if there is a problem with the dataset
     *                      or field name.
     */
    public static String getNumericFieldAsString(ResultSet dataSet, String fieldName) {
        if (dataSet==null || fieldName==null || fieldName.isEmpty()) return null;

        long value;
        try {
            value = dataSet.getLong(fieldName);
        } catch (SQLException e) {
            log.warn("Cannot find field {} in ResultSet", fieldName);
            return null;
        }

        return Long.toString(value);
    }

    /**
     * Return a field that is normally a timestamp value as a string
     *
     * @param dataSet       A dataset with a current record
     * @param fieldName     The number of the field to return
     * @param formatter     A DateTimeFormatter for converting the timestamp to a string
     * @return              The value of the field.  Null if there is a problem with the dataset
     *                      or field name.
     */
    public static String getTimestampFieldAsString(ResultSet dataSet, String fieldName, DateTimeFormatter formatter) {
        if (dataSet==null || fieldName==null || fieldName.isEmpty() || formatter == null) return null;

        String value = null;
        try {
            java.sql.Timestamp timestamp = dataSet.getTimestamp(fieldName);
            if (timestamp != null) {
                value = timestamp.toLocalDateTime().format(formatter);
            }

        } catch (SQLException e) {
            log.warn("Cannot find field {} in ResultSet", fieldName);
            return null;
        }

        return value;
    }

    /**
     * Return a field that is normally a timestamp value as a string
     *
     * @param dataSet       A dataset with a current record
     * @param fieldName     The number of the field to return
     * @return              The value of the field.  Null if there is a problem with the dataset
     *                      or field name.
     */
    public static String getTimestampFieldAsString(ResultSet dataSet, String fieldName) {
        return getTimestampFieldAsString(dataSet, fieldName, DateTimeFormatter.ISO_DATE_TIME);
    }

    /**
     * Return a fragment date (stored as separate day, month, and year short integer fields) as a string, where
     * the string has one of the following formats depending on which fields are present:
     *
     *      YYYY
     *      YYYY-MM
     *      YYYY-MM-DD
     *      -MM
     *      -MM-DD
     *      --DD
     *
     * @param dataSet       A dataset with a current record
     * @param dayField      The name of the day field
     * @param monthField    The name of the month field
     * @param yearField     The name of the year field
     * @return              The value of the field.  Null if there is a problem with the dataset
     *                      or field name.
     */
    public static String getFragmentedDateFieldsAsString(ResultSet dataSet, String dayField, String monthField, String yearField) {
        return getFragmentedDateFieldsAsString(dataSet, dayField, monthField, yearField, true);
    }

    /**
     * Return a fragment date (stored as separate day, month, and year short integer fields) as a string, where
     * the string has one of the following formats depending on which fields are present:
     *
     *      YYYY
     *      YYYY-MM
     *      YYYY-MM-DD
     *      -MM
     *      -MM-DD
     *      --DD
     *
     * @param dataSet               A dataset with a current record
     * @param dayField              The name of the day field
     * @param monthField            The name of the month field
     * @param yearField             The name of the year field
     * @param allowOnlyValidDates   True if only valid yeaR, month, and day fields are validated (note: leap years are considered)
     * @return                      The value of the field.  Null if there is a problem with the dataset
     *                              or field name.
     */
    public static String getFragmentedDateFieldsAsString(ResultSet dataSet, String dayField, String monthField, String yearField, Boolean allowOnlyValidDates) {
        int[] monthMayDays = new int[] {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (dataSet==null) {
            return null;
        }

        if (dayField!=null) {
            dayField = dayField.trim();
        }

        if (monthField!=null) {
            monthField = monthField.trim();
        }

        if (yearField!=null) {
            yearField = yearField.trim();
        }

        String value;
        try {
            Short year = null;
            Short month = null;
            Short day = null;

            if (yearField!=null && !yearField.isEmpty()) {
                year = dataSet.getShort(yearField);
                if (dataSet.wasNull() ||
                    (allowOnlyValidDates && (dataSet.wasNull() || year < 1 || year > 9999))) {
                    year = null;
                }
            }

            if (monthField!=null && !monthField.isEmpty()) {
                month = dataSet.getShort(monthField);
                if (dataSet.wasNull() ||
                    (allowOnlyValidDates && (dataSet.wasNull() || month<1 || month>12))) {
                    month = null;
                }
            }

            if (dayField!=null && !dayField.isEmpty()) {
                day = dataSet.getShort(dayField);
                if (dataSet.wasNull() ||
                    (allowOnlyValidDates && (dataSet.wasNull() || day<1 || (month!=null && day>monthMayDays[month])))) {
                    day = null;
                }
            }

            if (day == null && month == null && year == null) {
                return null;
            }

            value = "";
            if (year != null) {
                value = String.format("%04d", year);
            }

            if (month != null || day != null) {
                value += "-";
            }

            if (month != null) {
                value += String.format("%02d", month);
            }

            if (day != null) {
                value += "-" + String.format("%02d", day);
            }

        } catch (SQLException e) {
            log.warn("Cannot fragmented date fields {}, {}, {} from data set: {}", dayField, monthField, yearField, e.getMessage());
            return null;
        }

        return value;
    }

}
