package edu.usu.csse.sqltools;

import edu.usu.csse.strings.StringExtension;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.*;
import java.util.*;

/**
 * NamedPreparedStatement
 *
 * This is an adaptor for Java' PreparedStatement that accepts named parameters as placeholders instead of just
 * question marks, such as
 *
 *      SELECT * FOM PERSON WHERE last_name=@lastName AND age=@age
 *
 * The named parameters begin with an "@" and are followed by a valid identifier (using Java's rule for valid
 * identifiers).  A parameter can occur multiple times in a single statement. For * example:
 *
 *      SELECT weight * @facter, height * @facter FROM item
 */
@SuppressWarnings("WeakerAccess")
public class NamedPreparedStatement {
    private static final Logger log = LogManager.getLogger(NamedPreparedStatement.class.getName());

    private final PreparedStatement statement;
    private final Map<String, List<Integer>> indexMap;      // Maps parameter names to list of integers, which are
                                                            // the parameter indices.

    /**
    * Creates a NamedParameterStatement.  Wraps a call to {@link Connection#prepareStatement(String, int, int)
    * prepareStatement}.
    *
    * @param connection the database connection
    * @param query      the parameterized query
    * @throws SQLException if the statement could not be created
    */
    public NamedPreparedStatement(Connection connection, String query) throws
           SQLException {
       if(connection == null || query == null) throw new SQLException();
       indexMap= new HashMap<>();
       String parsedQuery=parse(query, indexMap);
       statement=connection.prepareStatement(parsedQuery, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
    }

    /**
     * @return      The SQL PreparedStatement
     */
    public PreparedStatement getPreparedStatement() {
        return statement;
    }

    /**
    * Sets a parameter.
    * @param name                       Parameter name (without the preceding @)
    * @param value                      Parameter value
    * @throws SQLException              If an error occurred
    * @throws IllegalArgumentException  If the parameter does not exist
    * @see PreparedStatement#setObject(int, Object)
    */
    public void setObject(String name, Object value) throws SQLException {
       List<Integer> indexes=getIndexes(name);
       for (Integer paramIndex : indexes) {
           statement.setObject(paramIndex, value);
       }
    }

    /**
    * Sets a parameter.
    * @param name                       parameter name
    * @param value                      Parameter value
    * @param targetSqlType              The SQL type (as defined in java.sql.Types) to be sent to the database.
    *                                   The scale argument may further qualify this type.
    * @throws SQLException              If an SQL error occurred
    * @throws IllegalArgumentException  if the parameter does not exist
    * @see PreparedStatement#setObject(int, Object, int)
    */
    public void setObject(String name, Object value, int targetSqlType) throws SQLException {
       List<Integer> indexes=getIndexes(name);
       for (Integer paramIndex : indexes) {
           statement.setObject(paramIndex, value, targetSqlType);
       }
    }

    /**
    * Sets a parameter fieldValue of any time
    * @param fieldName                  Parameter fieldName (without the preceding @)
    * @param fieldValue                 Parameter fieldValue
    * @param targetSqlType              The SQL type (as defined in java.sql.Types) to be sent to the database.
    *                                   The scale argument may further qualify this type.
    * @param scaleOrLength              For <code>java.sql.Types.DECIMAL</code> or <code>java.sql.Types.NUMERIC types</code>,
    *                                   this is the number of digits after the decimal point.
    *                                   For Java Object types <code>InputStream</code> and <code>Reader</code>,
    *                                   this is the length of the data in the stream or reader.
    *                                   For all other types, this fieldValue will be ignored.
    * @throws SQLException              If an SQL error occurred
    * @throws IllegalArgumentException  If the parameter does not exist
    * @see PreparedStatement#setObject(int, Object, int, int)
    */
    public void setObject(String fieldName, Object fieldValue, int targetSqlType, int scaleOrLength) throws SQLException {
       List<Integer> indexes=getIndexes(fieldName);
       for (Integer paramIndex : indexes) {
           if (fieldValue == null) {
               statement.setNull(paramIndex, targetSqlType);
           } else {
               statement.setObject(paramIndex, fieldValue, targetSqlType, scaleOrLength);
           }
       }
    }

    /**
    * Sets a parameter of a string type
    * @param fieldName                  Parameter fieldName (without the preceding @)
    * @param fieldValue                 Parameter fieldValue
    * @throws SQLException              If an SQL error occurred
    * @throws IllegalArgumentException  If the parameter does not exist
    * @see PreparedStatement#setString(int, String)
    */
    public void setString(String fieldName, String fieldValue) throws SQLException {
        setString(fieldName, fieldValue, false, false);
    }

    /**
     * Sets a parameter of a string type
     * @param fieldName                  Parameter fieldName (without the preceding @)
     * @param fieldValue                 Parameter fieldValue
     * @param trim                       True if the value should be trimmed
     * @param upperCase                  True if the value should be converted to upper cases
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setString(int, String)
     */
    public void setString(String fieldName, String fieldValue, boolean trim, boolean upperCase) throws SQLException {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue!=null && !fieldValue.isEmpty()) {
                if (trim) {
                    fieldValue = fieldValue.trim();
                }
                if (upperCase) {
                    fieldValue = fieldValue.toUpperCase();
                }
            }
            if (fieldValue==null || fieldValue.isEmpty()) {
                statement.setNull(paramIndex, Types.VARCHAR);
            } else {
                statement.setString(paramIndex, fieldValue);
            }
        }
    }


    /**
    * Sets a parameter fieldValue of an integer type
     *
    * @param fieldName                  Parameter fieldName (without the preceding @)
    * @param fieldValue                 Parameter fieldValue
    * @throws SQLException              If an SQL error occurred
    * @throws IllegalArgumentException  If the parameter does not exist
    * @see PreparedStatement#setInt(int, int)
    */
    public void setInt(String fieldName, Integer fieldValue) throws SQLException {
       List<Integer> indexes=getIndexes(fieldName);
       for (Integer paramIndex : indexes) {
           if (fieldValue == null) {
               statement.setNull(paramIndex, Types.INTEGER);
           } else {
               statement.setInt(paramIndex, fieldValue);
           }
       }
    }

    /**
    * Sets a parameter value of a short integer type
    * @param fieldName                  Parameter name (without the preceding @)
    * @param fieldValue                 Parameter value
    * @throws SQLException              If an SQL error occurred
    * @throws IllegalArgumentException  If the parameter does not exist
    * @see PreparedStatement#setShort(int, short)
    */
    public void setShort(String fieldName, Short fieldValue) throws SQLException {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.SMALLINT);
            } else {
                statement.setShort(paramIndex, fieldValue);
            }
        }
    }

    /**
     * Sets a parameter value of a long integer type
     *
     * @param fieldName                  Parameter name (without the preceding @)
     * @param fieldValue                 Parameter value
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setLong(int, long)
     */
    public void setLong(String fieldName, Long fieldValue) throws SQLException {
       List<Integer> indexes=getIndexes(fieldName);
       for (Integer paramIndex : indexes) {
           if (fieldValue == null) {
               statement.setNull(paramIndex, Types.BIGINT);
           } else {
               statement.setLong(paramIndex, fieldValue);
           }
       }
    }

    /**
     * Sets a parameter value of a double type
     *
     * @param fieldName                  Parameter name (without the preceding @)
     * @param fieldValue                 Parameter value
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setDouble(int, double)
     */
    public void setDouble(String fieldName, Double fieldValue) throws SQLException {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.DOUBLE);
            } else {
                statement.setDouble(paramIndex, fieldValue);
            }
        }
    }

    /**
     * Sets a parameter value of a float type
     * @param fieldName                  Parameter name (without the preceding @)
     * @param fieldValue                 Parameter value
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setFloat(int, float)
     */
    public void setFloat(String fieldName, Float fieldValue) throws SQLException {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.FLOAT);
            } else {
                statement.setFloat(paramIndex, fieldValue);
            }
        }
    }

    /**
     * Sets a parameter.
     * @param fieldName                  Parameter name (without the preceding @)
     * @param fieldValue                 Parameter value
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setBigDecimal(int, BigDecimal)
     */
    public void setBigDecimal(String fieldName, BigDecimal fieldValue) throws SQLException {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.NUMERIC);
            } else {
                statement.setBigDecimal(paramIndex, fieldValue);
            }
        }
    }

    /**
     * Sets a parameter value of a boolean type
     * @param fieldName                  Parameter name (without the preceding @)
     * @param fieldValue                 Parameter value
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setBoolean(int, boolean)
     */
    public void setBoolean(String fieldName, Boolean fieldValue) throws SQLException {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.BOOLEAN);
            } else {
                statement.setBoolean(paramIndex, fieldValue);
            }
        }
    }

    /**
     * Sets a parameter value of a Date type
     * @param fieldName                  Parameter name (without the preceding @)
     * @param fieldValue                 Parameter value
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setDate(int, Date)
     */
    public void setDate(String fieldName, Date fieldValue) throws SQLException
    {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.DATE);
            } else {
                statement.setDate(paramIndex, fieldValue);
            }
        }
    }

    /**
    * Sets a parameter fieldValue of a Time type
    * @param fieldName                  Parameter fieldName (without the preceding @)
    * @param fieldValue                 Parameter fieldValue
    * @throws SQLException              If an SQL error occurred
    * @throws IllegalArgumentException  If the parameter does not exist
    * @see PreparedStatement#setTime(int, Time)
    */
    public void setTime(String fieldName, Time fieldValue) throws SQLException
    {
       List<Integer> indexes=getIndexes(fieldName);
       for (Integer paramIndex : indexes) {
           if (fieldValue == null) {
               statement.setNull(paramIndex, Types.TIME);
           } else {
               statement.setTime(paramIndex, fieldValue);
           }
       }
    }

    /**
     * Sets a parameter fieldValue of a Timestamp type
     * @param fieldName                       Parameter fieldName (without the preceding @)
     * @param fieldValue                      Parameter fieldValue
     * @throws SQLException              If an SQL error occurred
     * @throws IllegalArgumentException  If the parameter does not exist
     * @see PreparedStatement#setTimestamp(int, Timestamp)
     */
    public void setTimestamp(String fieldName, Timestamp fieldValue) throws SQLException
    {
        List<Integer> indexes=getIndexes(fieldName);
        for (Integer paramIndex : indexes) {
            if (fieldValue == null) {
                statement.setNull(paramIndex, Types.TIMESTAMP);
            } else {
                statement.setTimestamp(paramIndex, fieldValue);
            }
        }
    }

    /**
     * Sets the designated parameter to SQL NULL.
     *
     * Note: You must specify the parameter's SQL type.
     *
     * @param name                      Parameter name (without the preceding @)
     * @param sqlType                   The SQL type code defined in java.sql.Types
     * @throws SQLException             If an SQL error occurred
     * @see PreparedStatement#setNull(int, int)
     */
    public void setNull(String name, int sqlType) throws SQLException {
        List<Integer> indexes=getIndexes(name);
        for (Integer paramIndex : indexes) {
            statement.setNull(paramIndex, sqlType);
        }
    }

    /**
     * Returns the indexes for a parameter.
     * @param name                       Parameter name (without the preceding @)
     * @return                           Parameter indexes
     * @throws IllegalArgumentException  If the parameter does not exist
     */
    public boolean containsParameter(String name) {
        return indexMap.get(name) != null;
    }

    /**
    * Executes the statement
     *
    * @return                           True if the first result is a {@link ResultSet}
    * @throws SQLException              If an SQL error occurred
    * @see PreparedStatement#execute()
    */
    public int execute() throws SQLException {
       boolean isResultSet = statement.execute();
       return ExecuteDbScript.getResultCount(isResultSet, statement);
    }

    /**
     * Executes the statement that is a query.
     *
     * @return the query results
     * @throws SQLException if an error occurred
     * @see PreparedStatement#executeQuery()
     */
    public ResultSet executeQuery() throws SQLException {
       return statement.executeQuery();
    }

    /**
     * @return  A string representations of the statement
     */
    @Override
    public String toString() {
        return statement.toString();
    }

    /**
     * Parses a query with named parameters.  The parameter-index mappings are put into the map, and the
     * parsed query is returned.  DO NOT CALL FROM CLIENT CODE.  This method is non-private so JUnit code can
     * test it.
     *
     * @param query    query to parse
     * @param paramMap map to hold parameter-index mappings
     * @return the parsed query
     */
    static String parse(String query, Map<String, List<Integer>> paramMap) throws SQLException {
        if (query==null)
            query = "";

        int length=query.length();
        StringBuilder parsedQuery=new StringBuilder(length);
        int index=1;

        log.trace("Raw query={}", query);

        int currentCharIndex=0;
        while (currentCharIndex<length) {
            if (currentCharIndex<length - 13 &&
                    query.startsWith("FROM stdin;", currentCharIndex)) {
                int endMarkerIndex = query.indexOf("\\.", currentCharIndex + 11);
                if (endMarkerIndex < 0) {
                    endMarkerIndex = length;
                } else {
                    endMarkerIndex += 2;
                }
                parsedQuery.append(query, currentCharIndex, endMarkerIndex);
                currentCharIndex = endMarkerIndex;
                continue;
            }

            if (currentCharIndex<length - 2 &&
                    query.startsWith("--", currentCharIndex)) {

                int endMarkerIndex = query.indexOf(System.lineSeparator(), currentCharIndex + 2);

                if (endMarkerIndex < 0) {
                    endMarkerIndex = length;
                } else {
                    endMarkerIndex += 1;
                }
                parsedQuery.append(query, currentCharIndex, endMarkerIndex);
                currentCharIndex = endMarkerIndex;
                continue;
            }

            char c=query.charAt(currentCharIndex);
            if (c == '\'' || c == '"' ) {
                int endMarkerIndex = StringExtension.findEndQuote(query, currentCharIndex) + 1;
                if (endMarkerIndex > query.length()) {
                    throw new SQLException("Query contains an unmatched quote");
                }
                parsedQuery.append(query, currentCharIndex, endMarkerIndex);
                currentCharIndex = endMarkerIndex;
                continue;
            }

            if(c=='@' && currentCharIndex < length-1 &&
                    Character.isJavaIdentifierStart(query.charAt(currentCharIndex+1))) {
                int endOfParameterLabel = currentCharIndex+2;
                while( endOfParameterLabel <length &&
                        Character.isJavaIdentifierPart(query.charAt(endOfParameterLabel))) {
                    endOfParameterLabel++;
                }
                String paramName=query.substring(currentCharIndex+1, endOfParameterLabel);

                List<Integer> indexList;
                if (!paramMap.containsKey(paramName)) {
                    indexList= new ArrayList<>();
                    paramMap.put(paramName, indexList);
                } else {
                    indexList = paramMap.get(paramName);
                }

                indexList.add(index);
                index++;

                c='?';                                  // replace @paramName with a question mark
                currentCharIndex = endOfParameterLabel-1;
            }

            parsedQuery.append(c);
            currentCharIndex++;
        }

        query = parsedQuery.toString();
        log.trace("Parsed query = {}", query);
        return query;
    }

    /**
     * Returns the indexes for a parameter.
     * @param name                       Parameter name (without the preceding @)
     * @return                           Parameter indexes
     * @throws IllegalArgumentException  If the parameter does not exist
     */
    private List<Integer> getIndexes(String name) {
        List<Integer> indexes = indexMap.get(name);
        if (indexes==null) {
            throw new IllegalArgumentException(String.format("Parameter %s not found in the supplied list of parameters", name));
        }
        return indexes;
    }
}
