package edu.usu.csse.sqltools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("WeakerAccess")
public class DbSequence {
    public static long getNextValue(Connection conn, String sequenceName) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT nextval(?)");
        statement.setString(1, sequenceName);
        return getValue(statement);
    }

    public static long getCurrentValue(Connection conn, String sequenceName) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT currval(?)");
        statement.setString(1, sequenceName);
        return getValue(statement);
    }

    public static void setValue(Connection conn, String sequenceName, long value) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT setval(?, ?)");
        statement.setString(1, sequenceName);
        statement.setLong(2, value);
        statement.execute();
    }

    private static long getValue(PreparedStatement statement) throws SQLException {
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next())
            throw new SQLException();
        return resultSet.getLong(1);
    }

}
